#FROM python:3.8.0-buster
FROM python:3.8.0-alpine

ARG BUILD_DEPS="alpine-sdk python3-dev libffi-dev libxml2-dev libxslt-dev zlib-dev openssl-dev"
RUN mkdir /vessel_info
WORKDIR /vessel_info
COPY requirements.txt requirements.txt

RUN apk add --no-cache ${BUILD_DEPS} \
    && sudo CFLAGS="-O0" pip install lxml \ 
    && pip install --no-cache-dir -r requirements.txt 

#RUN pip install -r requirements.txt

COPY . .

LABEL maintainer="Martand Singhal" \
    version="0.4"

# Set environment variables
#ENV FLASK_APP=app.py

# Expose the application's port
#EXPOSE 5000

#WORKDIR /vessel_info
# Run the application
#ENTRYPOINT ["/bin/bash"]
#CMD ["scrapy", "crawl", "marseille", "-o", "data1.json"]
ENTRYPOINT ["scrapyrt", "-i", "0.0.0.0"]
