# from sqlalchemy import create_engine, Column, Integer, String, DateTime, Float, Sequence
# from sqlalchemy.engine.url import URL
# from sqlalchemy.ext.declarative import declarative_base
# from vessel_info.settings import DATABASE
# from datetime import datetime

# DeclarativeBase = declarative_base()


# def db_connect():
#     """
#     Performs database connection using database settings from settings.py.
#     Returns sqlalchemy engine instance
#     """
#     return create_engine(URL(**DATABASE))


# def create_vessels_table(engine):
#     """"""
#     DeclarativeBase.metadata.create_all(engine)


# class Vessels(DeclarativeBase):
#     """Sqlalchemy vessels model"""

#     __tablename__ = "Vessels"

#     Id = Column(Integer, Sequence('vessels_id_seq', start=1, increment=1), primary_key=True)
#     Vessel_Name = Column("Vessel_Name", String, nullable=True)
#     AIS_Type = Column("AIS_Type", String, nullable=True)
#     Flag = Column("Flag", String, nullable=True)
#     Destination = Column("Destination", String, nullable=True)
#     ETA = Column("ETA", String, nullable=True)
#     IMO = Column("IMO", String, nullable=True)
#     MMSI = Column("MMSI", String, nullable=True)
#     Callsign = Column("Callsign", String, nullable=True)
#     Length = Column("Length", Float, nullable=True)
#     Beam = Column("Beam", Float, nullable=True)
#     Current_draught = Column("Current_draught", String, nullable=True)
#     Course = Column("Course", String, nullable=True)
#     Speed = Column("Speed", Float, nullable=True)
#     Coordinates = Column("Coordinates", String, nullable=True)
#     Last_report = Column("Last_report", String, nullable=True)
#     status = Column("Status", String, nullable=True)
#     timestamp = Column(
#         DateTime,
#         default=datetime.utcnow,
#         # onupdate=datetime.utcnow
#     )

