# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class VesselInfoPipeline(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    Vessel_Name = scrapy.Field()
    AIS_Type = scrapy.Field()
    Flag = scrapy.Field()
    Destination = scrapy.Field()
    ETA = scrapy.Field()
    IMO = scrapy.Field()
    MMSI = scrapy.Field()
    Callsign = scrapy.Field()
    Length = scrapy.Field()
    Beam = scrapy.Field()
    Current_draught = scrapy.Field()
    Course = scrapy.Field()
    Speed = scrapy.Field()
    Coordinates = scrapy.Field()
    Last_report = scrapy.Field()
    status = scrapy.Field()
