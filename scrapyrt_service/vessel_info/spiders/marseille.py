# -*- coding: utf-8 -*-
import scrapy
import re
from vessel_info.items import VesselInfoPipeline


class MarseilleSpider(scrapy.Spider):
    name = "marseille"
    allowed_domains = ["www.vesselfinder.com"]
    start_urls = ["https://www.vesselfinder.com/ports/MARSEILLE-FRANCE-1588/"]

    def start_requests(self):
        yield scrapy.Request(
            url="https://www.vesselfinder.com/ports/MARSEILLE-FRANCE-1588/",
            callback=self.parse,
        )

    def parse(self, response):
        for status in ["expected", "arrivals", "departures"]:
            for url in response.xpath(
                f'//section[@id="{status}"]//a[@class="named-item"]/@href'
            ).extract():
                yield response.follow(
                    url=url, callback=self.parse_pages, meta={"status": status}
                )

    def parse_pages(self, response):
        # create an instance of the items class defined in items.py
        vessel_pipe_item = VesselInfoPipeline()

        status = response.request.meta["status"]
        try:
            # extract the name of the vessel and the vessel field
            vessel_name = response.xpath(
                '//div[@class="col2 page"]/section[3]//table//tr[2]/td[2]//text()'
            ).getall()
            vessel_key = response.xpath(
                '//div[@class="col2 page"]/section[3]//table//tr[2]/td[1]//text()'
            ).getall()
            # add vessel name to the class 'items' instance
            vessel_pipe_item[vessel_key[0].replace(" ", "_")] = vessel_name[0]
        except IndexError:
            vessel_name_raw = response.xpath(
                '//h1[@class="title is-spaced is-uppercase"]//text()'
            ).getall()
            vessel_name = vessel_name_raw[0].split(" - ")
            vessel_pipe_item["Vessel_Name"] = vessel_name[0].lstrip()
        finally:
            # extract all other fields and corresponding values
            key_list = response.xpath(
                '//div[@class="column vfix-top npr"]//table[@class="tparams"]//td[1]//text()'
            ).getall()
            value_list = response.xpath(
                '//div[@class="column vfix-top npr"]//table[@class="tparams"]//td[2]//text()'
            ).getall()
            # clean the extracted fields and values and store in 'items' instance
            check_str = " / "
            if len(value_list) > len(key_list):
                value_list = value_list[: len(key_list)]
            for i in range(len(key_list)):
                # if i in range(len(key_list)):
                if check_str in key_list[i]:
                    new_keys = key_list[i].split(" / ")
                    new_values = value_list[i].split(" / ")
                    iter_values = iter(new_values)
                    for k in range(len(new_keys)):
                        clean_value = next(iter_values, None)
                        if clean_value in {None, "-", " - "}:
                            vessel_pipe_item[new_keys[k]] = None
                        else:
                            if new_keys[k] in {"Length", "Beam", "Speed"}:
                                vessel_pipe_item[new_keys[k]] = float(
                                    re.sub(" m| kn", "", clean_value)
                                )
                            else:
                                vessel_pipe_item[new_keys[k]] = clean_value
                else:
                    dict_key = key_list[i].rstrip().replace(" ", "_")
                    if value_list[i] in {"-", " - "}:
                        vessel_pipe_item[dict_key] = None
                    elif dict_key == "Position_received":
                        vessel_pipe_item["Last_report"] = value_list[i]
                    else:
                        try:
                            vessel_pipe_item[dict_key] = value_list[i]
                        except KeyError as e:
                            print(e)

            # add the vessel status to the class 'items' instance
            vessel_pipe_item["status"] = status

            return vessel_pipe_item


table_dict = dict()
