# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

# from sqlalchemy.orm import sessionmaker
# from vessel_info.models import Vessels, db_connect, create_vessels_table


# class VesselInfoPipeline(object):
# """Vessel_info pipeline for storing scraped items in the database"""
# def __init__(self):
#     """
#     Initializes database connection and sessionmaker.
#     Creates deals table.
#     """
#     engine = db_connect()
#     create_vessels_table(engine)
#     self.Session = sessionmaker(bind=engine)

# def process_item(self, item, spider):
#     """Save vessels in the database.

#     This method is called for every item pipeline component.

#     """
#     session = self.Session()
#     vessel = Vessels(**item)

#     try:
#         session.add(vessel)
#         session.commit()
#     except:
#         session.rollback()
#         raise
#     finally:
#         session.close()

#     return item

