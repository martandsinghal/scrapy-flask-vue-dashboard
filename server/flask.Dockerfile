FROM python:3.8.0-alpine

#FROM python:3.8.0-buster

RUN mkdir /flask_backend
WORKDIR /flask_backend

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1


COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . .

LABEL maintainer="Martand Singhal" \
    version="0.3"

# Set environment variables
#ENV FLASK_APP=app.py

# Expose the application's port
#EXPOSE 5000

# Run the application
#CMD ["flask", "run", "--host=0.0.0.0", "--port=5000"]
CMD gunicorn -b 0.0.0.0:5000 app:app
#CMD flask run --host=0.0.0.0 --port=5000
