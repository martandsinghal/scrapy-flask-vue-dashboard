from flask import Flask
from .config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_caching import Cache

# from datetime import datetime
from flask_cors import CORS
from .process_data import vega_format, geo_vega
import requests
import json

# instantiate the app
app = Flask(__name__)
app.config.from_object(Config)
cache = Cache(app)

# enable CORS
CORS(app, resources={r"/*": {"origins": "*"}})

# create the db object and write the corresponding class
db = SQLAlchemy(app)


from app.models import Vessels


@app.route("/data")
@cache.cached(timeout=570)
def hello_world():

    params = {"spider_name": "marseille", "start_requests": "true"}
    response = requests.get("http://scrapy_server:9080/crawl.json", params)
    resp_data = response.json()["items"]
    try:
        # db.session.execute("Truncate Vessels RESTART IDENTITY")
        db.session.query(Vessels).delete()
        db.session.commit()
        db.session.execute("ALTER SEQUENCE vessels_id_seq RESTART WITH 1")
        db.session.commit()
    except:
        db.session.rollback()
    for item in resp_data:
        try:
            vessel = Vessels(**item)
            db.session.add(vessel)
            db.session.commit()
        except:
            db.session.rollback()

    try:
        # status chart
        status_data = (
            db.session.query(Vessels.status, db.func.count(Vessels.status))
            # .filter(Vessels.status == "departures")
            .group_by(Vessels.status).all()
        )
        status_list = [
            {"status": k, "n_ships": v} for (k, v) in dict(status_data).items()
        ]

        # ship type chart data
        raw_type_dict = {"Type": list(db.session.query(Vessels.AIS_Type))}
        type_list = vega_format(raw_type_dict, color_palette="purple")

        # speed chart data
        raw_speed_dict = {"Speed": list(db.session.query(Vessels.Speed))}
        speed_list = vega_format(raw_speed_dict, color_palette="purple")

        # length chart data
        raw_length_dict = {"Length": list(db.session.query(Vessels.Length))}
        length_list = vega_format(raw_length_dict, color_palette="grey")

        # list for geo map
        raw_name_dict = {"Name": list(db.session.query(Vessels.Vessel_Name))}
        raw_coord_dict = {"Coord": list(db.session.query(Vessels.Coordinates))}
        raw_status_dict = {"Status": list(db.session.query(Vessels.status))}
        raw_dict = {
            **raw_name_dict,
            **raw_type_dict,
            **raw_coord_dict,
            **raw_length_dict,
            **raw_speed_dict,
            **raw_status_dict,
        }
        geo_list = geo_vega(raw_dict)

        latest_timestamp = list(db.session.query(Vessels.timestamp))[0][0].isoformat()
        latest_id = list(db.session.query(Vessels.Id))[0][0]
        final_list = [
            status_list,
            type_list,
            speed_list,
            length_list,
            geo_list,
            {"latest_timestamp": latest_timestamp},
            {"latest_id": latest_id},
        ]
        js_obj = json.dumps(final_list)
    except:
        js_obj = "an error occured"
    return js_obj


if __name__ == "__main__":
    app.run()
