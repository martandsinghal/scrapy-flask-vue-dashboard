import re

#  helper functions for data processing to transform to vega 'data' specifications


def vega_format(raw_dict, *, color_palette):
    if color_palette == "purple":
        color_list = purple_color_list
    else:
        color_list = grey_color_list
    clean_list = clean_data(raw_dict)
    print(clean_list)
    if "Type" in raw_dict:
        input_iterable = set(clean_list)
        count_key = True
    else:
        if "Speed" in raw_dict:
            bin_list = speed_bins
        elif "Length" in raw_dict:
            bin_list = length_bins
        input_iterable = bin_list
        count_key = False
    processed_list = [
        {
            "count": count_val(clean_list, item, count_key),
            "val": item,
            "color": color_list[idx],
        }
        for idx, item in enumerate(input_iterable)
    ]
    print(processed_list)
    if "Type" in raw_dict:
        final_list = processed_list
    else:
        final_list = update_count_vals(clean_list, bin_list, processed_list)
    return final_list


def geo_vega(dict_of_lists):
    clean_dict_of_lists = {
        key: clean_data({key: value}, geo_key=True)
        for key, value in dict_of_lists.items()
    }
    for i, co_list in enumerate(clean_dict_of_lists["Coord"]):
        if co_list is not None:
            for k, co_el in enumerate(co_list):
                new_list = co_el.split(" ")
                if (new_list[1] == "N") or (new_list[1] == "E"):
                    clean_dict_of_lists["Coord"][i][k] = float(new_list[0])
                else:
                    clean_dict_of_lists["Coord"][i][k] = -1 * float(new_list[0])
    geo_list = [
        {k: v for k, v in zip([*clean_dict_of_lists.keys()], value_tuple)}
        for value_tuple in list(zip(*clean_dict_of_lists.values()))
    ]
    clean_geo_list = [item for item in geo_list if item["Coord"] is not None]
    return clean_geo_list


def clean_data(raw_dict, *, geo_key=False):
    clean_list = []
    for item in list(*raw_dict.values()):
        if item[0] is not None:
            if "Type" in raw_dict:
                clean_list.append(item[0].split(" ")[0])
            elif "Coord" in raw_dict:
                clean_list.append(item[0].split("/"))
            else:
                clean_list.append(item[0])
        elif geo_key:
            clean_list.append(None)
    return clean_list


def count_val(clean_list, item, count_key):
    if count_key:
        return clean_list.count(item)
    else:
        return 0


def update_count_vals(clean_list, bin_list, processed_list):
    for d in clean_list:
        if d <= process_bins(bin_list[0], 1):
            processed_list[0]["count"] += 1
            print(processed_list[0]["count"])
        elif process_bins(bin_list[1], 0) < d <= process_bins(bin_list[1], 1):
            processed_list[1]["count"] += 1
        elif process_bins(bin_list[2], 0) < d <= process_bins(bin_list[2], 1):
            processed_list[2]["count"] += 1
            print(processed_list[2]["count"])
        elif process_bins(bin_list[3], 0) < d <= process_bins(bin_list[3], 1):
            processed_list[3]["count"] += 1
        elif process_bins(bin_list[4], 0) < d <= process_bins(bin_list[4], 1):
            processed_list[4]["count"] += 1
        elif d > process_bins(bin_list[5], 1):
            processed_list[5]["count"] += 1
    return processed_list


def process_bins(str_val, idx):
    return int(re.split("[=≤>-]", str_val)[idx])


# color palettes and bins for Vega chart bars

purple_color_list = [
    "#e1bee7",
    "#ce93d8",
    "#ba68c8",
    "#ab47bc",
    "#9c27b0",
    "#8e24aa",
    "#7b1fa2",
    "#6a1b9a",
    "#4a148c",
    "#ea80fc",
    "#e040fb",
    "#d500f9",
    "#aa00ff",
]

grey_color_list = [
    "#e6e6e6",
    "#d5d5d5",
    "#c4c4c4",
    "#b3b3b3",
    "#a2a2a2",
    "#919191",
    "#6e6e6e",
    "#5c5c5c",
    "#4a4a4a",
    "#383838",
]

speed_bins = ["=0", "0-1", "1-5", "5-10", "10-15", ">15"]
length_bins = ["≤60", "60-120", "120-180", "180-240", "240-300", ">300"]
