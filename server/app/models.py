from datetime import datetime
from app import db


class Vessels(db.Model):

    """Sqlalchemy vessels model"""

    __tablename__ = "Vessels"

    Id = db.Column(
        db.Integer,
        db.Sequence("vessels_id_seq", start=1, increment=1),
        primary_key=True,
    )
    Vessel_Name = db.Column("Vessel_Name", db.String, nullable=True)
    AIS_Type = db.Column("AIS_Type", db.String, nullable=True)
    Flag = db.Column("Flag", db.String, nullable=True)
    Destination = db.Column("Destination", db.String, nullable=True)
    ETA = db.Column("ETA", db.String, nullable=True)
    IMO = db.Column("IMO", db.String, nullable=True)
    MMSI = db.Column("MMSI", db.String, nullable=True)
    Callsign = db.Column("Callsign", db.String, nullable=True)
    Length = db.Column("Length", db.Float, nullable=True)
    Beam = db.Column("Beam", db.Float, nullable=True)
    Current_draught = db.Column("Current_draught", db.String, nullable=True)
    Course = db.Column("Course", db.String, nullable=True)
    Speed = db.Column("Speed", db.Float, nullable=True)
    Coordinates = db.Column("Coordinates", db.String, nullable=True)
    Last_report = db.Column("Last_report", db.String, nullable=True)
    status = db.Column("Status", db.String, nullable=True)
    timestamp = db.Column(
        db.DateTime,
        default=datetime.utcnow,
        # onupdate=datetime.utcnow
    )
