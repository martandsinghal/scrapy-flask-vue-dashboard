# commands for entering postgres db and accessing data:
From Mac terminal type:
docker exec -it database_postgres bash
where database_postgres is the postgres container name

enter command: 
psql -U user vessel_db
Prompt will ask for password --- enter password
prompt will look like this: vessel_db=#
This means you are inside the db
now enter the command:
select * from vessels;

commands for running scrapy spider: 
cd to spiders folder inside vessel_info
now execute the following from terminal:
scrapy crawl marseille -o data.json

commands for running flask app:
cd to workspace
export FLASK_APP=app.py
python3 -m flask run

Run sqlalchemy queries inside the container:
exec the bash of python container
run python3
import app (or from app import *)
now run the query:
app.Vessels.query.order_by(app.Vessels.Vessel_Name)

to check the namespace of app module:
app.__dir__()

curl "http://localhost:9080/crawl.json?spider_name=marseille&start_requests=true"

connect to digitalocean with private ssh key -- id_rsa1 (this is not the default ssh key)
ssh -i ~/.ssh/id_rsa1 root@207.154.205.68

remember to allow ufw for nginx 
see:
https://www.digitalocean.com/community/tutorials/ufw-essentials-common-firewall-rules-and-commands
